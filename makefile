USE_SYSTEMD = 0
DEBUG = 0
PREFIX = /usr/local

all: not_mpc config.h

.PHONY: clean uninstall install install-systemd help

SRC = main.c

ifneq ($(DEBUG), 0)
CFLAGS += -DDEBUG -g
endif

CFLAGS += -D_DEFAULT_SOURCE
CFLAGS += `pkg-config --cflags --libs libnotify`    # last tested version 0.7.7
CFLAGS += `pkg-config --cflags --libs libmpdclient` # last tested version 2.15

ifneq ($(USE_SYSTEMD), 0)
	CFLAGS += `pkg-config --cflags --libs libsystemd` # last tested version 239.0
	SRC += systemd/systemd_doop.c
else
	SRC += systemd/systemd_noop.c
endif

not_mpc: $(SRC) config.h
	@echo
	@if test $(DEBUG) != "0" ;then\
		echo 'Debug   enabled';\
	else\
		echo 'Debug   disabled';\
	fi
	@if test $(USE_SYSTEMD) != "0" ;then\
		echo 'Systemd enabled';\
	else\
		echo 'Systemd disabled';\
	fi
	@echo
	$(CC) $(SRC) $(CFLAGS) -o $@

config.h:
	cp config.def.h config.h

install:
	@echo
	@echo 'Install prefix' \"$(PREFIX)\"
	@echo
	umask 0022
	@cp -v ./not_mpc $(PREFIX)/bin
	umask 0233
	@cp -v ./not_mpc.1 $(PREFIX)/share/man/man1

install-systemd: install
	@if test $(PREFIX) = "/usr" ;then\
		cp -v systemd/package.service /usr/lib/systemd/user/not-mpc.service;\
	else\
		cp -v systemd/local.service /etc/systemd/user/not-mpc.service;\
	fi\

uninstall:
	@echo
	@echo 'Uninstall prefix' \"$(PREFIX)\"
	@echo
	rm -f $(PREFIX)/bin/not_mpc
	rm -f $(PREFIX)/share/man/man1/not_mpc.1
	rm -f /usr/lib/systemd/user/not-mpc.service
	rm -f /etc/systemd/user/not-mpc.service

clean:
	rm not_mpc

help:
	@echo
	@echo build targets:
	@echo 'all           - builds not_mpc'
	@echo 'not_mpc       - builds not_mpc'
	@echo 'clean         - remove all built files'
	@echo
	@echo install targets:
	@echo 'uninstall       - remove all possibly installed files'
	@echo 'install         - install not_mpc and man pages'
	@echo 'install-systemd - install not_mpc, man pages, and not-mpd.service'
	@echo 'Use targets like this "make clean"'
	@echo
	@echo build options:
	@echo 'USE_SYSTEMD   - set to 1 to enable systemd support'
	@echo 'PREFIX        - where to install'
	@echo 'DEBUG         - set to 1 to enable debug output'
	@echo 'Use options like this "make PREFIX=/usr/local DEBUG=1"'
	@echo
	@echo config files:
	@echo 'config.h     - mpd and other settings go here'
	@echo 'Check man -l not_mpd.1 for an into'




