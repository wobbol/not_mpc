#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <systemd/sd-daemon.h>

void do_sd_notify(int unset_environment, char *msg)
{
	sd_notify(unset_environment, msg);
}

void do_sd_notifyf(int unset_environment, char *format, ...)
{
	char buf[256]; /* RATS: ignore */
	va_list ap;

	va_start(ap, format);
	vsnprintf(buf, 256, format, ap); /* RATS: ignore */
	va_end(ap);
	buf[255] = '\0';

	sd_notify(unset_environment, buf);
}
