#include <inttypes.h>

#define NOTIFY_BUF_SZ 256

#ifdef DEBUG
	#define dputs(str) puts(str)
#else
	#define dputs(str)
#endif

struct {
	const int16_t timeout_ms;
	const int16_t port;
	const char *host;
} const mpd_server_settings = {
	.timeout_ms = 10000,
	.port = 6600,
	.host = "localhost",
};

struct {
	const char *playing;
	const char *paused;
	const char *stopped;
	const char *unknown;
	const char *empty_str;
} const str_local = {
	.playing = "Playing",
	.paused =  "Paused",
	.stopped = "Stopped",
	.unknown = "Unknown",
	.empty_str = "!empty!",
};
