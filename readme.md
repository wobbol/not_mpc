# not_mpc

notification from mpd on song change, play, pause, or stop
configure through config.h.  
To exit `not_mpc` use Ctrl-c.

## Dependancies

- libnotify
- libmpdclient
- libsystemd (optional via `make USE_SYSTEMD=0`)

## Build
`make help` is avalable.

### No Systemd
```sh
$ make config.h
$ nano config.h
$ make
```
### Systemd
```sh
$ make config.h
$ nano config.h
$ make USE_SYSTEMD=1
```

## Install and Run
### No systemd
 ```sh
$ make install
$ not_mpc
```
### Systemd
```sh
$ make install-systemd
$ systemd --user enable not-mpc.service
$ systemd --user start not-mpc.service
 ```
