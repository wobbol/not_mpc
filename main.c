/* gcc -o hello_world `pkg-config --cflags --libs libnotify` hello_world.c */
#include <libnotify/notify.h>
#include <mpd/client.h>
#include <inttypes.h>
#include <stdarg.h>
#include <string.h>

#include "config.h"
#include "systemd/systemd.h"

#ifdef DEBUG
#define eprint_current_song debug_print_current_song
#define nullprint debug_nullprint
#else
#define eprint_current_song()
#define nullprint(val) val
#endif

struct mpd_connection *mpd_conn = NULL;
int run;

struct {
	unsigned int q_pos;
	unsigned int q_len;
	unsigned int length;
	unsigned int elapsed;
	char *title;
	char *artist;
	enum mpd_state audio_state;
} current_song;

void debug_print_current_song(void)
{
	fprintf(stderr, "  queue_pos: %lu\n", current_song.q_pos);
	fprintf(stderr, "  queue_len: %lu\n", current_song.q_len);
	fprintf(stderr, "     length: %lu\n", current_song.length);
	fprintf(stderr, "    elapsed: %lu\n", current_song.elapsed);
	fprintf(stderr, "      title: %s\n",  current_song.title);
	fprintf(stderr, "     artist: %s\n",  current_song.artist);
	fprintf(stderr, "audio_state: %u\n",  current_song.audio_state);
}

#define debug_nullprint(val) \
do {\
	fprintf(stderr, "%s ", #val);\
	if(val)\
		fprintf(stderr,"good\n");\
	else\
		fprintf(stderr,"null\n");\
} while(0);

char *strdup_or_empty(const char *s)
{
	if(s)
		return strdup(s);
	else
		return strdup(str_local.empty_str);
}

void get_current_song(void)
{
	mpd_command_list_begin(mpd_conn, true);
	mpd_send_status(mpd_conn);
	mpd_send_current_song(mpd_conn);
	nullprint(mpd_command_list_end(mpd_conn));
	struct mpd_status *status = mpd_recv_status(mpd_conn);
	mpd_response_next(mpd_conn);
	struct mpd_song *song = mpd_recv_song(mpd_conn);
	mpd_response_finish(mpd_conn);
	nullprint(status);
	nullprint(song);
	free(current_song.title);
	free(current_song.artist);

	if(!song) {
		memset(&current_song, 0, sizeof(current_song));
		return;
	}
	current_song.audio_state = mpd_status_get_state(status);
	current_song.q_pos       = mpd_status_get_song_pos(status);
	current_song.q_len       = mpd_status_get_queue_length(status);
	current_song.length      = mpd_status_get_total_time(status);
	current_song.elapsed     = mpd_status_get_elapsed_time(status);
	current_song.title       = strdup_or_empty(mpd_song_get_tag(song, MPD_TAG_TITLE, 0));
	current_song.artist      = strdup_or_empty(mpd_song_get_tag(song, MPD_TAG_ARTIST, 0));
	mpd_status_free(status);
	mpd_song_free(song);
}

void init_mpd(void)
{
	mpd_conn = mpd_connection_new(
		mpd_server_settings.host,
		mpd_server_settings.port,
		mpd_server_settings.timeout_ms
	);
	if(MPD_ERROR_SUCCESS == mpd_connection_get_error(mpd_conn))
		return;
	/* TODO: check this string in other languages */
	fprintf(stderr, "%s\n", mpd_connection_get_error_message(mpd_conn));
	exit(1);
}

void init(void)
{
	notify_init("MPD thing");
	init_mpd();
}
void uninit(void)
{
	notify_uninit();
	mpd_connection_free(mpd_conn);
}

void do_notify(const char *summary, const char *text, const char *icon)
{
	NotifyNotification *note = notify_notification_new (summary, text, icon);
	notify_notification_set_urgency(note, NOTIFY_URGENCY_LOW);
	notify_notification_show (note, NULL);
	g_object_unref(G_OBJECT(note));
}

void do_notify_current_song(void)
{
	char buf[NOTIFY_BUF_SZ] = {0}; /* RATS: ignore */
	char *b = buf;
	char *e = &buf[NOTIFY_BUF_SZ - 1];
	b += snprintf(b, e - b,
		"%lu of %lu\n"
		"<time>[%lu/%lu]\n"
		"%s --> %s\n",
		current_song.q_pos,   current_song.q_len,
		current_song.elapsed, current_song.length,
		current_song.artist,  current_song.title
	);
	const char *title;
	switch(current_song.audio_state) {
		case MPD_STATE_PLAY:  title = str_local.playing; break;
		case MPD_STATE_PAUSE: title = str_local.paused;  break;
		case MPD_STATE_STOP:  title = str_local.stopped;  break;
		default:              title = str_local.unknown;
	}
	do_notify(title, buf, NULL);
	if(current_song.audio_state == MPD_STATE_STOP) {
		do_sd_notifyf(0, "READY=1\nSTATUS=%s", title);
	} else {
		do_sd_notifyf(0,
			"READY=1\nSTATUS=%s: %s --> %s",
			title, current_song.artist, current_song.title
		);
	}
}
void sighandle(int s)
{
	run = 0;
	mpd_send_noidle(mpd_conn);
}

void install_sighandle(void)
{
	struct sigaction sa;
	sa.sa_handler = sighandle;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;
	if(-1 == sigaction(SIGINT, &sa, NULL)) {
		perror("sigaction()");
		exit(1);
	}
}

int main() {
	run = 1;
	install_sighandle();
	init();
	do_sd_notify(0, "READY=1\nSTATUS=Waiting for event from mpd.");
	while(run && mpd_run_idle_mask(mpd_conn, MPD_IDLE_PLAYER)) {
		get_current_song();
		eprint_current_song();
		do_notify_current_song();
	}
	do_sd_notify(0, "STOPPING=1\nSTATUS=Exiting");
	uninit();
	do_sd_notify(0, "STATUS=Exited");
	return 0;
}
